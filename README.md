![Angular Modules](Angular Modules-01.svg){width=251 height=156}

# AngularModules

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

Run 
```
ng build @fingerprintsoft/angular-keycloak
ng build @fingerprintsoft/angular-spring-hal
ng build @fingerprintsoft/ionic-angular-keycloak
``` 

to build the sub-projects individually

## Publishing to npm registry
- Make sure you are logged in using `npm login`
- Make sure the versions each project is updated using `npm version {new_version_number}`
- Build the project that needs to be published using the `--prod` flag. e.g 
`ng build @fingerprintsoft/angular-spring-hal --prod`
- `cd` into `dist` directory e.g `cd dist/fingerprintsoft/angular-spring-hal/`. 
  The full directory path should be printed in the terminal at the end of the build command.
- Run `npm publish`

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
