import { BrowserTab } from '@ionic-native/browser-tab/ngx';
import { Platform } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { first } from 'rxjs/operators';

import { NgZone } from '@angular/core';
import { Plugins } from '@capacitor/core';
import { Subject } from 'rxjs';
import {
  KeycloakAdapter,
  KeycloakConfig,
  KeycloakLoginOptions, KeycloakLogoutOptions,
  KeycloakService,
} from '@fingerprintsoft/angular-keycloak';

const {App} = Plugins;

export class IonicKeycloakService extends KeycloakService {

  private platform: Platform;
  private browserTab: BrowserTab;
  private inAppBrowser: InAppBrowser;
  private zone: NgZone;

  private deeplinkCallback = new Subject<string>();

  constructor(
    config: string | KeycloakConfig,
    platform: Platform,
    browserTab: BrowserTab,
    inAppBrowser: InAppBrowser,
    zone: NgZone,
  ) {
    super(config);
    this.platform = platform;
    this.browserTab = browserTab;
    this.inAppBrowser = inAppBrowser;
    this.zone = zone;
  }


  loadAdapter(type?): KeycloakAdapter {
    if (type == 'cordova') {
      //TODO: cordova options are not correct here
      this.loginIframe.enable = false;
      let shallowCloneCordovaOptions = (userOptions) => {
        if (userOptions && userOptions.cordovaOptions) {
          return Object.keys(userOptions.cordovaOptions).reduce((options, optionName) => {
            options[optionName] = userOptions.cordovaOptions[optionName];
            return options;
          }, {});
        } else {
          return {};
        }
      };

      let formatCordovaOptions = (cordovaOptions) => {
        return Object.keys(cordovaOptions).reduce((options, optionName) => {
          options.push(optionName + "=" + cordovaOptions[optionName]);
          return options;
        }, []).join(",");
      };

      let createCordovaOptions = (userOptions) => {
        let cordovaOptions: any = shallowCloneCordovaOptions(userOptions);
        cordovaOptions.location = 'no';
        if (userOptions && userOptions.prompt == 'none') {
          cordovaOptions.hidden = 'yes';
        }
        return formatCordovaOptions(cordovaOptions);
      };

      this.initialiseAppOpenURLListener();

      return {
        login: (options?: KeycloakLoginOptions) => {
          let promise = this.createPromise();
          let cordovaOptions = createCordovaOptions(options);
          let loginUrl = this.createLoginUrl(options);
          let browser = this.inAppBrowser.create(loginUrl, '_blank', options)
          let completed = false;

          let closed = false;
          let closeBrowser = () => {
            closed = true;
            browser.close();
          };

          //TODO: Something with platform
          browser.on('loadstart').subscribe(event => {
            if (event.url.indexOf('http://localhost') == 0) {
              let callback = this.parseCallback(event.url);
              this.processCallback(callback, promise);
              closeBrowser();
              completed = true;
            }
          })
          browser.on('loaderror').subscribe(event => {
            if (!completed) {
              if (event.url.indexOf('http://localhost') == 0) {
                let callback = this.parseCallback(event.url);
                this.processCallback(callback, promise);
                closeBrowser();
                completed = true;
              } else {
                promise.setError();
                closeBrowser();
              }
            }
          })
          browser.on('exit').subscribe(event => {
            if (!closed) {
              promise.setError({
                reason: "closed_by_user",
              });
            }
          })
          return promise.promise;
        },

        logout: (options?: KeycloakLogoutOptions) => {
          let promise = this.createPromise();
          let logoutUrl = this.createLogoutUrl(options);
          let browser = this.inAppBrowser.create(logoutUrl, '_blank', 'location=no,hidden=yes')

          let error;

          browser.on('loadstart').subscribe(event => {
            if (event.url.indexOf('http://localhost') == 0) {
              browser.close();
            }

          })
          browser.on('loaderror').subscribe(event => {
            if (event.url.indexOf('http://localhost') == 0) {
              browser.close();
            } else {
              error = true;
              browser.close();
            }
          })
          browser.on('exit').subscribe(event => {
            if (error) {
              promise.setError();
            } else {
              this.clearToken();
              promise.setSuccess();
            }
          })
          return promise.promise;
        },

        register: (options?: KeycloakLoginOptions) => {
          let promise = this.createPromise();
          let registerUrl = this.createRegisterUrl();
          let cordovaOptions = createCordovaOptions(options);
          let browser = this.inAppBrowser.create(registerUrl, '_blank', cordovaOptions)
          browser.on('loadstart').subscribe(event => {
            if (event.url.indexOf('http://localhost') == 0) {
              browser.close();
              let oauth = this.parseCallback(event.url);
              this.processCallback(oauth, promise);
            }

          })
          return promise.promise;
        },

        accountManagement: () => {
          let accountUrl = this.createAccountUrl();
          if (typeof accountUrl !== 'undefined') {
            let browser = this.inAppBrowser.create(accountUrl, '_blank', 'location=no')
            browser.on('loadstart').subscribe(event => {
              if (event.url.indexOf('http://localhost') == 0) {
                browser.close();
              }

            })
          } else {
            throw "Not supported by the OIDC server";
          }
          return this.createPromise().promise;
        },

        redirectUri: (options) => {
          return 'http://localhost';
        },
      }
    }

    if (type == 'cordova-native') {
      this.loginIframe.enable = false;

      this.initialiseAppOpenURLListener();

      return {

        login: (options?: KeycloakLoginOptions) => {
          let promise = this.createPromise();
          let loginUrl = this.createLoginUrl(options);

          //TODO: Something with platform
          this.deeplinkCallback
            .pipe(first())
            .subscribe(
              dlm => {
                console.log(`Got the deep link in login`)
                this.browserTab.isAvailable().then(available => {
                    if (available) {
                      this.browserTab.close()
                    }
                  },
                )
                if (dlm) {
                  console.log(`url: ${dlm}`)
                  const oauth = this.parseCallback(dlm);
                  this.processCallback(oauth, promise);
                }

              },
              error => {
                this.browserTab.isAvailable().then(available => {
                    if (available) {
                      this.browserTab.close()
                    }
                  },
                )
              },
            )
          this.browserTab.isAvailable().then(available => {
              if (available) {
                this.browserTab.openUrl(loginUrl);
              }
            },
          )

          return promise.promise;
        },

        logout: (options: KeycloakLogoutOptions) => {
          let promise = this.createPromise();
          let logoutUrl = this.createLogoutUrl(options);
          this.deeplinkCallback
            .pipe(first())
            .subscribe(dlm => {
              console.log(`Got the deep link in logout`)
              this.browserTab.isAvailable().then(available => {
                  if (available) {
                    this.browserTab.close();
                  }
                },
              )
              this.clearToken();
              promise.setSuccess();
            });

          this.browserTab.isAvailable().then(available => {
              if (available) {
                this.browserTab.openUrl(logoutUrl);
              }
            },
          )

          return promise.promise;
        },

        register: (options: KeycloakLoginOptions) => {
          let promise = this.createPromise();
          let registerUrl = this.createRegisterUrl(options);
          //TODO: Something with platform
          this.deeplinkCallback
            .pipe(first())
            .subscribe(dlm => {
              console.log(`Got the deep link in register`)
              this.browserTab.isAvailable().then(available => {
                  if (available) {
                    this.browserTab.close()
                  }
                },
              )
              if (dlm) {
                console.log(`url: ${dlm}`)
                const oauth = this.parseCallback(dlm);
                this.processCallback(oauth, promise);
              }

            });

          this.browserTab.isAvailable().then(available => {
              if (available) {
                this.browserTab.openUrl(registerUrl);
              }
            },
          )

          return promise.promise;

        },

        accountManagement: () => {
          let accountUrl = this.createAccountUrl();
          if (typeof accountUrl !== 'undefined') {
            this.browserTab.isAvailable().then(available => {
                if (available) {
                  this.browserTab.openUrl(accountUrl);
                }
              },
            )

          } else {
            throw "Not supported by the OIDC server";
          }
          return this.createPromise().promise;
        },

        redirectUri: (options) => {
          if (options && options.redirectUri) {
            return options.redirectUri;
          } else if (this.redirectUri) {
            return this.redirectUri;
          } else {
            return "http://localhost";
          }
        },
      }
    }

    return super.loadAdapter(type);
  }

  private addDeepLinkCallback(deeplink: string) {
    this.deeplinkCallback.next(deeplink);
  }


  protected initialiseAppOpenURLListener() {
    App.addListener('appUrlOpen', (data: any) => {
      this.zone.run(() => {
        // Example url: https://orders.fingerprintsoft.co/home
        // slug = /home
        console.log(`The data url is  ${data.url}`)
        this.addDeepLinkCallback(data.url)
        // const slug = data.url.split(".co").pop();
        // if (slug) {
        //     this.router.navigateByUrl(slug);
        // }
        // If no match, do nothing - let regular routing
        // logic take over
      });
    });
  }

}
