import { Injectable, NgZone } from '@angular/core';
import { Platform } from '@ionic/angular';
import { BrowserTab } from '@ionic-native/browser-tab/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { IonicKeycloakService } from './ionic-keycloak.service';
import { AuthService, KeycloakOptions } from '@fingerprintsoft/angular-keycloak';


@Injectable({
  providedIn: 'root',
})
export class IonicAuthService extends AuthService {

  constructor(
    private platform: Platform,
    private browserTab: BrowserTab,
    private inAppBrowser: InAppBrowser,
    private zone: NgZone,
  ) {
    super();
  }

  async init(options: KeycloakOptions = {}) {
    this.initServiceValues(options);
    const {config, initOptions} = options;

    this.keycloak = new IonicKeycloakService(
      config,
      this.platform,
      this.browserTab,
      this.inAppBrowser,
      this.zone,
    );
    this.bindKeycloakEvents();

    console.log(`in javascript app init`)
    const authenticated = await this.keycloak.init(initOptions);

    if (authenticated && this.loadUserProfileAtStartUp) {
      await this.loadUserProfile();
    }

    return authenticated;
  }
}
