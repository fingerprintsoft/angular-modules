import { forwardRef, ModuleWithProviders, NgModule } from '@angular/core';
import { AngularKeycloakModule, BearerInterceptor } from '@fingerprintsoft/angular-keycloak';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';


@NgModule({
  imports: [
    HttpClientModule,
    AngularKeycloakModule
  ],
})
export class IonicAngularKeycloakModule {
  static forRoot(): ModuleWithProviders<IonicAngularKeycloakModule> {
    return {
      ngModule: IonicAngularKeycloakModule,
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: forwardRef(() => BearerInterceptor),
          multi: true,
        },
      ],

    }
  }
}
