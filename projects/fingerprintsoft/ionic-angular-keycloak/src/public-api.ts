/*
 * Public API Surface of ionic-angular-keycloak
 */
export * from './lib/ionic-angular-keycloak.module';
export * from './lib/services';
