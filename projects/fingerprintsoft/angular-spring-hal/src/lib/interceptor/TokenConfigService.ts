import {Injectable, InjectionToken} from "@angular/core";
import {TokenConfig} from "../models/TokenConfig";

export const TokenConfigService = new InjectionToken<TokenConfig>("TokenConfig");
