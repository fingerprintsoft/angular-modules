import { ModuleWithProviders, NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { ExternalService } from './services/external.service';
import { ResourceService } from './services/resource.service';
import { TokenConfig } from './models/TokenConfig';
import { AuthInterceptor } from './interceptor/AuthInterceptor';
import { TokenConfigService } from './interceptor/TokenConfigService';


@NgModule({
  imports: [HttpClientModule],
  declarations: [],
  exports: [HttpClientModule],
  providers: [
    ExternalService,
    HttpClient,
    {
      provide: ResourceService,
      useClass: ResourceService,
      deps: [ExternalService],
    }],
})
export class AngularHalModule {
  static forRoot(tokenConfig?: TokenConfig): ModuleWithProviders<AngularHalModule> {
    return {
      ngModule: AngularHalModule,
      providers: [
        ExternalService,
        HttpClient,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: AuthInterceptor,
          multi: true,
          deps: [TokenConfigService],
        },
        {
          provide: TokenConfigService,
          useValue: tokenConfig == null ? '' : tokenConfig,
        },
        {
          provide: ResourceService,
          useClass: ResourceService,
          deps: [ExternalService],
        },
      ],
    };
  }
}
