import {
  KeycloakAdapterName,
  KeycloakFlow,
  KeycloakOnLoad,
  KeycloakPkceMethod,
  KeycloakResponseMode,
} from './constants';

export interface KeycloakConfig {
  /**
   * URL to the KeycloakService server, for example: http://keycloak-server/auth
   */
  url?: string;
  /**
   * Name of the realm, for example: 'myrealm'
   */
  realm: string;
  /**
   * Client identifier, example: 'myapp'
   */
  clientId: string;
}

export interface KeycloakInitOptions {
  /**
   * Adds a [cryptographic nonce](https://en.wikipedia.org/wiki/Cryptographic_nonce)
   * to verify that the authentication response matches the request.
   * @default true
   */
  useNonce?: boolean;

  /**
   * Allows to use different adapter:
   *
   * - {string} default - using browser api for redirects
   * - {string} cordova - using cordova plugins
   * - {function} - allows to provide custom function as adapter.
   */
  adapter?: KeycloakAdapterName;

  /**
   * Specifies an action to do on load.
   */
  onLoad?: KeycloakOnLoad;

  /**
   * Set an initial value for the token.
   */
  token?: string;

  /**
   * Set an initial value for the refresh token.
   */
  refreshToken?: string;

  /**
   * Set an initial value for the id token (only together with `token` or
   * `refreshToken`).
   */
  idToken?: string;

  /**
   * Set an initial value for skew between local time and KeycloakService server in
   * seconds (only together with `token` or `refreshToken`).
   */
  timeSkew?: number;

  /**
   * Set to enable/disable monitoring login state.
   * @default true
   */
  checkLoginIframe?: boolean;

  /**
   * Set the interval to check login state (in seconds).
   * @default 5
   */
  checkLoginIframeInterval?: number;

  /**
   * Set the OpenID Connect response mode to send to KeycloakService upon login.
   * @default fragment After successful authentication KeycloakService will redirect
   *                   to JavaScript application with OpenID Connect parameters
   *                   added in URL fragment. This is generally safer and
   *                   recommended over query.
   */
  responseMode?: KeycloakResponseMode;

  /**
   * Specifies a default uri to redirect to after login or logout.
   * This is currently supported for adapter 'cordova-native' and 'default'
   */
  redirectUri?: string;

  /**
   * Specifies an uri to redirect to after silent check-sso.
   * Silent check-sso will only happen, when this redirect uri is given and
   * the specified uri is available whithin the application.
   */
  silentCheckSsoRedirectUri?: string;

  /**
   * Set the OpenID Connect flow.
   * @default standard
   */
  flow?: KeycloakFlow;

  /**
   * Configures the Proof Key for Code Exchange (PKCE) method to use.
   * The currently allowed method is 'S256'.
   * If not configured, PKCE will not be used.
   */
  pkceMethod?: KeycloakPkceMethod;

  /**
   * Enables logging messages from KeycloakService to the console.
   * @default false
   */
  enableLogging?: boolean
}

export interface KeycloakLoginOptions {
  /**
   * Undocumented.
   */
  scope?: string;

  /**
   * Specifies the uri to redirect to after login.
   */
  redirectUri?: string;

  /**
   * By default the login screen is displayed if the user is not logged into
   * KeycloakService. To only authenticate to the application if the user is already
   * logged in and not display the login page if the user is not logged in, set
   * this option to `'none'`. To always require re-authentication and ignore
   * SSO, set this option to `'login'`.
   */
  prompt?: 'none' | 'login';

  /**
   * If value is `'register'` then user is redirected to registration page,
   * otherwise to login page.
   */
  action?: string;

  /**
   * Used just if user is already authenticated. Specifies maximum time since
   * the authentication of user happened. If user is already authenticated for
   * longer time than `'maxAge'`, the SSO is ignored and he will need to
   * authenticate again.
   */
  maxAge?: number;

  /**
   * Used to pre-fill the username/email field on the login form.
   */
  loginHint?: string;

  /**
   * Used to tell KeycloakService which IDP the user wants to authenticate with.
   */
  idpHint?: string;

  /**
   * Sets the 'ui_locales' query param in compliance with section 3.1.2.1
   * of the OIDC 1.0 specification.
   */
  locale?: string;

  /**
   * Specifies arguments that are passed to the Cordova in-app-browser (if applicable).
   * Options 'hidden' and 'location' are not affected by these arguments.
   * All available options are defined at https://cordova.apache.org/docs/en/latest/reference/cordova-plugin-inappbrowser/.
   * Example of use: { zoom: "no", hardwareback: "yes" }
   */
  cordovaOptions?: { [optionName: string]: string };
}

export interface KeycloakLogoutOptions {
  /**
   * Specifies the uri to redirect to after logout.
   */
  redirectUri?: string;
}

/**
 * HTTP Methods
 */
export type HttpMethods =
  | 'GET'
  | 'POST'
  | 'PUT'
  | 'DELETE'
  | 'OPTIONS'
  | 'HEAD'
  | 'PATCH';

/**
 * ExcludedUrl type may be used to specify the url and the HTTP method that
 * should not be intercepted by the KeycloakBearerInterceptor.
 *
 * Example:
 * const excludedUrl: ExcludedUrl[] = [
 *  {
 *    url: 'reports/public'
 *    httpMethods: ['GET']
 *  }
 * ]
 *
 * In the example above for URL reports/public and HTTP Method GET the
 * bearer will not be automatically added.
 *
 * If the url is informed but httpMethod is undefined, then the bearer
 * will not be added for all HTTP Methods.
 */
export interface ExcludedUrl {
  url: string;
  httpMethods?: HttpMethods[];
}

/**
 * Similar to ExcludedUrl, contains the HTTP methods and a regex to
 * include the url patterns.
 * This interface is used internally by the KeycloakService.
 */
export interface ExcludedUrlRegex {
  urlPattern: RegExp;
  httpMethods?: HttpMethods[];
}

/**
 * keycloak-angular initialization options.
 */
export interface KeycloakOptions {
  /**
   * Configs to init the keycloak-js library. If undefined, will look for a keycloak.json file
   * at root of the project.
   * If not undefined, can be a string meaning the url to the keycloak.json file or an object
   * of {@link KeycloakConfig}. Use this configuration if you want to specify the keycloak server,
   * realm, clientId. This is usefull if you have different configurations for production, stage
   * and development environments. Hint: Make use of Angular environment configuration.
   */
  config?: string | KeycloakConfig;
  /**
   * Options to initialize the Keycloak adapter, matches the options as provided by Keycloak itself.
   */
  initOptions?: KeycloakInitOptions;
  /**
   * By default all requests made by Angular HttpClient will be intercepted in order to
   * add the bearer in the Authorization Http Header. However, if this is a not desired
   * feature, the enableBearerInterceptor must be false.
   *
   * Briefly, if enableBearerInterceptor === false, the bearer will not be added
   * to the authorization header.
   *
   * The default value is true.
   */
  enableBearerInterceptor?: boolean;
  /**
   * Forces the execution of loadUserProfile after the keycloak initialization considering that the
   * user logged in.
   * This option is recommended if is desirable to have the user details at the beginning,
   * so after the login, the loadUserProfile function will be called and it's value cached.
   *
   * The default value is true.
   */
  loadUserProfileAtStartUp?: boolean;
  /**
   * String Array to exclude the urls that should not have the Authorization Header automatically
   * added. This library makes use of Angular Http Interceptor, to automatically add the Bearer
   * token to the request.
   */
  bearerExcludedUrls?: (string | ExcludedUrl)[];
  /**
   * This value will be used as the Authorization Http Header name. The default value is
   * **Authorization**. If the backend expects requests to have a token in a different header, you
   * should change this value, i.e: **JWT-Authorization**. This will result in a Http Header
   * Authorization as "JWT-Authorization: bearer <token>".
   */
  authorizationHeaderName?: string;
  /**
   * This value will be included in the Authorization Http Header param. The default value is
   * **bearer**, which will result in a Http Header Authorization as "Authorization: bearer <token>".
   * If any other value is needed by the backend in the authorization header, you should change this
   * value, i.e: **Bearer**.
   *
   * Warning: this value must be in compliance with the keycloak server instance and the adapter.
   */
  bearerPrefix?: string;
}
