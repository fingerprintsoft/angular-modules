import { KeycloakLoginOptions, KeycloakLogoutOptions } from './configuration';

export declare type KeycloakPromiseCallback<T> = (result: T) => void;

export class KeycloakPromise<TSuccess, TError> extends Promise<TSuccess> {
  /**
   * Function to call if the promised action succeeds.
   *
   * @deprecated Use `.then()` instead.
   */
  success?(callback: KeycloakPromiseCallback<TSuccess>): KeycloakPromise<TSuccess, TError>;

  /**
   * Function to call if the promised action throws an error.
   *
   * @deprecated Use `.catch()` instead.
   */
  error?(callback: KeycloakPromiseCallback<TError>): KeycloakPromise<TSuccess, TError>;
}

export interface KeycloakError {
  error: string;
  error_description: string;
}

export interface KeycloakAdapter {
  login(options?: KeycloakLoginOptions): KeycloakPromise<void, void>;

  logout(options?: KeycloakLogoutOptions): KeycloakPromise<void, void>;

  register(options?: KeycloakLoginOptions): KeycloakPromise<void, void>;

  accountManagement(): KeycloakPromise<void, void>;

  redirectUri(options?: { redirectUri: string; } | KeycloakLogoutOptions | KeycloakLoginOptions, encodeHash?: boolean): string;
}

export interface KeycloakProfile {
  id?: string;
  username?: string;
  email?: string;
  firstName?: string;
  lastName?: string;
  enabled?: boolean;
  emailVerified?: boolean;
  totp?: boolean;
  createdTimestamp?: number;
}

export interface KeycloakTokenParsed {
  exp?: number;
  iat?: number;
  nonce?: string;
  sub?: string;
  session_state?: string;
  realm_access?: KeycloakRoles;
  resource_access?: KeycloakResourceAccess;
}

export interface KeycloakResourceAccess {
  [key: string]: KeycloakRoles
}

export interface KeycloakRoles {
  roles: string[];
}

export class LocalStorage {

  constructor() {
    localStorage.setItem('this.keycloakInstance-test', 'test');
    localStorage.removeItem('this.keycloakInstance-test');
  }

  clearExpired() {
    let time = new Date().getTime();
    for (let i = 0; i < localStorage.length; i++) {
      let key = localStorage.key(i);
      if (key && key.indexOf('this.keycloakInstance-callback-') == 0) {
        let value = localStorage.getItem(key);
        if (value) {
          try {
            let expires = JSON.parse(value).expires;
            if (!expires || expires < time) {
              localStorage.removeItem(key);
            }
          } catch (err) {
            localStorage.removeItem(key);
          }
        }
      }
    }
  }

  get(state) {
    if (!state) {
      return;
    }

    let key = 'this.keycloakInstance-callback-' + state;
    let value = localStorage.getItem(key);
    if (value) {
      localStorage.removeItem(key);
      value = JSON.parse(value);
    }

    this.clearExpired();
    return value;
  };

  add(state) {
    this.clearExpired();

    let key = 'this.keycloakInstance-callback-' + state.state;
    state.expires = new Date().getTime() + (60 * 60 * 1000);
    localStorage.setItem(key, JSON.stringify(state));
  };
}

export class CookieStorage {

  get(state) {
    if (!state) {
      return;
    }

    let value = this.getCookie('this.keycloakInstance-callback-' + state);
    this.setCookie('this.keycloakInstance-callback-' + state, '', this.cookieExpiration(-100));
    if (value) {
      return JSON.parse(value);
    }
  };

  add(state) {
    this.setCookie('this.keycloakInstance-callback-' + state.state, JSON.stringify(state), this.cookieExpiration(60));
  };

  removeItem(key) {
    this.setCookie(key, '', this.cookieExpiration(-100));
  };

  private cookieExpiration(minutes) {
    let exp = new Date();
    exp.setTime(exp.getTime() + (minutes * 60 * 1000));
    return exp;
  };

  private getCookie(key) {
    let name = key + '=';
    let ca = document.cookie.split(';');
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return '';
  };

  private setCookie(key, value, expirationDate) {
    let cookie = key + '=' + value + '; '
      + 'expires=' + expirationDate.toUTCString() + '; ';
    document.cookie = cookie;
  }
}
