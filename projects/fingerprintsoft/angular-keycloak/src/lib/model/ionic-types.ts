export interface Browser {
}

export interface AppBrowser {
  /**
   * Opens a URL in a new InAppBrowser instance, the current browser instance, or the system browser.
   * @param  url {string}     The URL to load.
   * @param  target {string}  The target in which to load the URL, an optional parameter that defaults to _self.
   * @param  options {string} Options for the InAppBrowser. Optional, defaulting to: location=yes.
   *                 The options string must not contain any blank space, and each feature's
   *                 name/value pairs must be separated by a comma. Feature names are case insensitive.
   * @returns InAppBrowserObject
   */
  create(url: string, target: string, options: string): any;
}

export interface Browser {
  /**
   * Check if BrowserTab option is available
   * @return {Promise<any>} Returns a promise that resolves when check is successful and returns true or false
   */
  isAvailable(): Promise<any>;
  /**
   * Opens the provided URL using a browser tab
   * @param {string} url  The URL you want to open
   * @return {Promise<any>} Returns a promise that resolves when check open was successful
   */
  openUrl(url: string): Promise<any>;
  /**
   * Closes browser tab
   * @return {Promise<any>} Returns a promise that resolves when close was finished
   */
  close(): Promise<any>;
}
