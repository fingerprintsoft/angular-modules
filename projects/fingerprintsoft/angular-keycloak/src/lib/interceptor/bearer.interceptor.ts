import { forwardRef, Inject, Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';

import { from, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { AuthService } from '../services';
import { ExcludedUrlRegex } from '../model';

/**
 * This interceptor includes the bearer by default in all HttpClient requests.
 *
 * If you need to exclude some URLs from adding the bearer, please, take a look
 * at the {@link KeycloakOptions} bearerExcludedUrls property.
 *
 * You will have to use a provider
 *
 * @NgModule({
 *    ...
 *    providers: [
 *      ...,
 *      {provide: AuthService , useClass: IonicAuthService }
 *    ]
 * })
 */
@Injectable()
export class BearerInterceptor implements HttpInterceptor {
  constructor(@Inject(forwardRef(() => AuthService))private authService: AuthService) {
  }

  /**
   * Checks if the url is excluded from having the Bearer Authorization
   * header added.
   *
   * @param req http request from @angular http module.
   * @param excludedUrlRegex contains the url pattern and the http methods,
   * excluded from adding the bearer at the Http Request.
   */
  private isUrlExcluded(
    {method, url}: HttpRequest<any>,
    {urlPattern, httpMethods}: ExcludedUrlRegex,
  ): boolean {
    const httpTest =
      httpMethods.length === 0 ||
      httpMethods.join().indexOf(method.toUpperCase()) > -1;

    const urlTest = urlPattern.test(url);

    return httpTest && urlTest;
  }

  /**
   * Intercept implementation that checks if the request url matches the excludedUrls.
   * If not, adds the Authorization header to the request if the user is logged in.
   *
   * @param req
   * @param next
   */
  public intercept(req: HttpRequest<any>, next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    const enableBearerInterceptor = this.authService.isEnableBearerInterceptor();
    const excludedUrls = this.authService.getExcludedUrls();
    if (!enableBearerInterceptor) {
      return next.handle(req);
    }

    const shallPass: boolean = excludedUrls.findIndex(item => this.isUrlExcluded(req, item)) > -1;
    if (shallPass) {
      return next.handle(req);
    }

    return from(this.authService.isLoggedIn())
      .pipe(
        mergeMap((loggedIn: boolean) => loggedIn ? this.handleRequestWithTokenHeader(req, next) : next.handle(req)),
      );
  }

  /**
   * Adds the token of the current user to the Authorization header
   *
   * @param req
   * @param next
   */
  private handleRequestWithTokenHeader(
    req: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<any> {
    return this.authService.addTokenToHeader(req.headers)
      .pipe(
        mergeMap(headersWithBearer => {
          const kcReq = req.clone({headers: headersWithBearer});
          return next.handle(kcReq);
        }),
      );
  }

}
