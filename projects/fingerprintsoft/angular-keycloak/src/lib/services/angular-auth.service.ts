import { Injectable } from '@angular/core';
import { KeycloakService } from './keycloak.service';
import { KeycloakOptions } from '../model';
import { AuthService } from './abstract-auth.service';


@Injectable({
  providedIn: 'root',
})
export class AngularAuthService extends AuthService {

  constructor() {
    super();
  }

  async init(options: KeycloakOptions = {}) {
    this.initServiceValues(options);
    const {config, initOptions} = options;

    this.keycloak = new KeycloakService(
      config,
    );
    this.bindKeycloakEvents();

    console.log(`in javascript app init`)
    const authenticated = await this.keycloak.init(initOptions);

    if (authenticated && this.loadUserProfileAtStartUp) {
      await this.loadUserProfile();
    }

    return authenticated;
  }
}
