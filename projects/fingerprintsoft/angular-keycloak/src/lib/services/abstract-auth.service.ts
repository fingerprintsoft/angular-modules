import { from, Observable, Subject } from 'rxjs';
import { KeycloakService } from './keycloak.service';
import {
  AuthEvent,
  AuthEventType,
  ExcludedUrl,
  ExcludedUrlRegex,
  KeycloakLoginOptions,
  KeycloakOptions,
  KeycloakProfile,
} from '../model';
import { HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';


export abstract class AuthService {

  /**
   * The bearer prefix that will be appended to the Authorization Header.
   */
  private bearerPrefix: string;
  /**
   * Value that will be used as the Authorization Http Header name.
   */
  private authorizationHeaderName: string;
  /**
   * The excluded urls patterns that must skip the KeycloakBearerInterceptor.
   */
  private excludedUrls: ExcludedUrlRegex[];
  /**
   * Flag to indicate if the bearer will not be added to the authorization header.
   */
  private enableBearerInterceptor: boolean;

  /**
   * When the implicit flow is choosen there must exist a silentRefresh, as there is
   * no refresh token.
   */
  private silentRefresh: boolean;
  /**
   * Indicates that the user profile should be loaded at the keycloak initialization,
   * just after the login.
   */
  protected loadUserProfileAtStartUp: boolean;
  /**
   * User profile as KeycloakProfile interface.
   */
  private userProfile: KeycloakProfile;

  /**
   * Observer for the keycloak events
   */
  private authEvents: Subject<AuthEvent> = new Subject<AuthEvent>();

  protected keycloak: KeycloakService;

  constructor() {
  }

  async abstract init(options: KeycloakOptions);

  async login(options: KeycloakLoginOptions = {}) {
    await this.keycloak.login(options);

    if (this.loadUserProfileAtStartUp) {
      await this.loadUserProfile();
    }
  }

  async logout(redirectUri?: string) {
    await this.keycloak.logout({
      redirectUri: redirectUri,
    })
    this.userProfile = undefined;
  }

  isAuthenticated() {
    return this.keycloak.isAuthenticated();
  }

  getUserProfile(): Observable<KeycloakProfile> {
    return from(this.keycloak.loadUserProfile());
  }

  /**
   * Loads the user profile.
   * Returns promise to set functions to be invoked if the profile was loaded
   * successfully, or if the profile could not be loaded.
   *
   * @param forceReload
   * If true will force the loadUserProfile even if its already loaded.
   * @returns
   * A promise with the KeycloakProfile data loaded.
   */
  public async loadUserProfile(forceReload = false) {
    if (this.userProfile && !forceReload) {
      return this.userProfile;
    }

    if (!this.keycloak.isAuthenticated()) {
      throw new Error(
        'The user profile was not loaded as the user is not logged in.',
      );
    }

    return (this.userProfile = await this.keycloak.loadUserProfile());
  }

  /**
   * Returns the logged username.
   *
   * @returns
   * The logged username.
   */
  public getUsername() {
    if (!this.userProfile) {
      throw new Error('User not logged in or user profile was not loaded.');
    }

    return this.userProfile.username;
  }

  /**
   * Check if the user has access to the specified role. It will look for roles in
   * realm and clientId, but will not check if the user is logged in for better performance.
   *
   * @param role
   * role name
   * @param resource
   * resource name If not specified, `clientId` is used
   * @returns
   * A boolean meaning if the user has the specified Role.
   */
  isUserInRole(role: string, resource?: string): boolean {
    return this.keycloak.isUserInRole(role, resource);
  }

  /**
   * Return the roles of the logged user. The allRoles parameter, with default value
   * true, will return the clientId and realm roles associated with the logged user. If set to false
   * it will only return the user roles associated with the clientId.
   *
   * @param allRoles
   * Flag to set if all roles should be returned.(Optional: default value is true)
   * @returns
   * Array of Roles associated with the logged user.
   */
  getUserRoles(allRoles: boolean = true): string[] {
    return this.keycloak.getUserRoles(allRoles);
  }

  /**
   * Check if user is logged in.
   *
   * @returns
   * A boolean that indicates if the user is logged in.
   */
  async isLoggedIn(): Promise<boolean> {
    try {
      if (!this.keycloak.isAuthenticated()) {
        return false;
      }
      await this.keycloak.updateToken(20);
      return true;
    } catch (error) {
      return false;
    }
  }

  /**
   * Adds a valid token in header. The key & value format is:
   * Authorization Bearer <token>.
   * If the headers param is undefined it will create the Angular headers object.
   *
   * @param headers
   * Updated header with Authorization and Keycloak token.
   * @returns
   * An observable with with the HTTP Authorization header and the current token.
   */
  public addTokenToHeader(headers: HttpHeaders = new HttpHeaders()) {
    return from(this.getToken()).pipe(
      map((token) =>
        headers.set(this.authorizationHeaderName, this.bearerPrefix + token),
      ),
    );
  }

  /**
   * Returns the authenticated token, calling updateToken to get a refreshed one if
   * necessary. If the session is expired and the forceLogin flag is set to true,
   * this method calls the login method for a new login, otherwise rejects.
   *
   * @param forceLogin
   * Flag whether a login should be enforced if the session is expired.
   * @returns
   * Promise with the generated token.
   */
  async getToken(forceLogin = true): Promise<string> {
    try {
      await this.keycloak.updateToken(10);
      return this.keycloak.getToken();
    } catch (error) {
      if (forceLogin) {
        await this.login();
      } else {
        throw error;
      }
    }
  }

  /**
   * Returns the excluded URLs that should not be considered by
   * the http interceptor which automatically adds the authorization header in the Http Request.
   *
   * @returns
   * The excluded urls that must not be intercepted by the KeycloakBearerInterceptor.
   */
  getExcludedUrls(): ExcludedUrlRegex[] {
    return this.excludedUrls;
  }

  /**
   * Flag to indicate if the bearer will be added to the authorization header.
   *
   * @returns
   * Returns if the bearer interceptor was set to be disabled.
   */
  isEnableBearerInterceptor(): boolean {
    return this.enableBearerInterceptor;
  }

  /**
   * Loads all bearerExcludedUrl content in a uniform type: ExcludedUrl,
   * so it becomes easier to handle.
   *
   * @param bearerExcludedUrls array of strings or ExcludedUrl that includes
   * the url and HttpMethod.
   */
  private loadExcludedUrls(bearerExcludedUrls: (string | ExcludedUrl)[]): ExcludedUrlRegex[] {
    const excludedUrls: ExcludedUrlRegex[] = [];
    for (const item of bearerExcludedUrls) {
      let excludedUrl: ExcludedUrlRegex;
      if (typeof item === 'string') {
        excludedUrl = {urlPattern: new RegExp(item, 'i'), httpMethods: []};
      } else {
        excludedUrl = {
          urlPattern: new RegExp(item.url, 'i'),
          httpMethods: item.httpMethods,
        };
      }
      excludedUrls.push(excludedUrl);
    }
    return excludedUrls;
  }

  /**
   * Handles the class values initialization.
   *
   * @param options
   */
  protected initServiceValues({
                                enableBearerInterceptor = true,
                                loadUserProfileAtStartUp = true,
                                bearerExcludedUrls = [],
                                authorizationHeaderName = 'Authorization',
                                bearerPrefix = 'bearer',
                                initOptions,
                              }: KeycloakOptions): void {
    this.enableBearerInterceptor = enableBearerInterceptor;
    this.loadUserProfileAtStartUp = loadUserProfileAtStartUp;
    this.authorizationHeaderName = authorizationHeaderName;
    this.bearerPrefix = bearerPrefix.trim().concat(' ');
    this.excludedUrls = this.loadExcludedUrls(bearerExcludedUrls);
    this.silentRefresh = initOptions ? initOptions.flow === 'implicit' : false;
  }

  /**
   * Binds the keycloak-js events to the keycloakEvents Subject
   * which is a good way to monitor for changes, if needed.
   *
   * The keycloakEvents returns the keycloak-js event type and any
   * argument if the source function provides any.
   */
  protected bindKeycloakEvents(): void {
    this.keycloak.onAuthError = (errorData) => {
      this.authEvents.next({
        args: errorData,
        type: AuthEventType.OnAuthError,
      });
    };

    this.keycloak.onAuthLogout = () => {
      console.debug("Authenticated successfully")
      this.authEvents.next({type: AuthEventType.OnAuthLogout});
    };

    this.keycloak.onAuthRefreshSuccess = () => {
      console.debug("Authenticated successfully")
      this.authEvents.next({
        type: AuthEventType.OnAuthRefreshSuccess,
      });
    };

    this.keycloak.onAuthRefreshError = () => {
      this.authEvents.next({
        type: AuthEventType.OnAuthRefreshError,
      });
    };

    this.keycloak.onAuthSuccess = () => {
      console.debug("Authenticated successfully")
      this.authEvents.next({type: AuthEventType.OnAuthSuccess});
    };

    this.keycloak.onTokenExpired = () => {
      this.authEvents.next({
        type: AuthEventType.OnTokenExpired,
      });
    };

    this.keycloak.onReady = (authenticated) => {
      this.authEvents.next({
        args: authenticated,
        type: AuthEventType.OnReady,
      });
    };
  }

}
