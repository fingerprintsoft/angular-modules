import { sha256 } from 'js-sha256';
import * as base64js from 'base64-js';
import {
  CookieStorage,
  KeycloakAdapter,
  KeycloakConfig,
  KeycloakError,
  KeycloakFlow,
  KeycloakInitOptions,
  KeycloakLoginOptions,
  KeycloakLogoutOptions,
  KeycloakPkceMethod,
  KeycloakProfile,
  KeycloakPromise,
  KeycloakResourceAccess,
  KeycloakResponseMode,
  KeycloakResponseType,
  KeycloakRoles,
  KeycloakTokenParsed,
  LocalStorage,
} from '../model';

export class KeycloakService {

  private adapter: KeycloakAdapter;
  private refreshQueue = [];
  private callbackStorage: LocalStorage | CookieStorage;
  private scripts;
  private readonly iframeVersion;
  private useNonce = true;
  private readonly logInfo;
  private readonly logWarn;

  private tokenTimeoutHandle;
  private endpoints;

  protected loginIframe = {
    enable: true,
    callbackList: [],
    interval: 5,
    iframeOrigin: undefined,
    iframe: undefined,

  };
  private enableLogging: boolean = false;
  private flow: KeycloakFlow;
  private responseMode: KeycloakResponseMode;
  protected redirectUri: string;
  private authServerUrl: string;
  private realm: string;
  private clientId: string;
  private responseType: KeycloakResponseType;
  private pkceMethod: KeycloakPkceMethod;
  private realmAccess: KeycloakRoles;
  private resourceAccess: KeycloakResourceAccess;
  private profile: KeycloakProfile;
  private userInfo: {}; // KeycloakUserInfo;;
  private tokenParsed: KeycloakTokenParsed;
  private refreshToken: string;
  private timeSkew: number;
  private token: string;
  private refreshTokenParsed: KeycloakTokenParsed;
  private idTokenParsed: KeycloakTokenParsed;
  private loginRequired: boolean;
  private idToken: string;
  private sessionId: string;
  private authenticated: boolean;
  private subject: string;
  private silentCheckSsoRedirectUri: string;
  private readonly config: string | KeycloakConfig;

  public onReady?(authenticated?: boolean): void;

  public onAuthSuccess?(): void;

  public onAuthError?(errorData?: KeycloakError): void;

  public onAuthRefreshSuccess?(): void;

  public onAuthRefreshError?(): void;

  public onAuthLogout?(): void;

  public onActionUpdate?(status: 'success' | 'cancelled' | 'error'): void;

  public onTokenExpired?(): void;

  constructor(
    config: string | KeycloakConfig,
  ) {
    this.config = config;
    this.scripts = document.getElementsByTagName('script');
    for (let i = 0; i < this.scripts.length; i++) {
      if ((this.scripts[i].src.indexOf('keycloak.js') !== -1 || this.scripts[i].src.indexOf('keycloak.min.js') !== -1) && this.scripts[i].src.indexOf('version=') !== -1) {
        this.iframeVersion = this.scripts[i].src.substring(this.scripts[i].src.indexOf('version=') + 8).split('&')[0];
      }
    }
    this.logInfo = this.createLogger(console.info);
    this.logWarn = this.createLogger(console.warn);

  }

  init(initOptions: KeycloakInitOptions): KeycloakPromise<boolean, KeycloakError> {
    this.authenticated = false;

    this.callbackStorage = this.createCallbackStorage();
    let adapters = ['default', 'cordova', 'cordova-native'];

    if (initOptions && adapters.indexOf(initOptions.adapter) > -1) {
      this.adapter = this.loadAdapter(initOptions.adapter);
    } else if (initOptions && typeof initOptions.adapter === "object") {
      this.adapter = initOptions.adapter;
    } else {
      this.adapter = this.loadAdapter();
    }


    if (initOptions) {
      if (typeof initOptions.useNonce !== 'undefined') {
        this.useNonce = initOptions.useNonce;
      }

      if (typeof initOptions.checkLoginIframe !== 'undefined') {
        this.loginIframe.enable = initOptions.checkLoginIframe;
      }

      if (initOptions.checkLoginIframeInterval) {
        this.loginIframe.interval = initOptions.checkLoginIframeInterval;
      }

      if (initOptions.onLoad === 'login-required') {
        this.loginRequired = true;
      }

      if (initOptions.responseMode) {
        if (initOptions.responseMode === 'query' || initOptions.responseMode === 'fragment') {
          this.responseMode = initOptions.responseMode;
        } else {
          throw 'Invalid value for responseMode';
        }
      }

      if (initOptions.flow) {
        switch (initOptions.flow) {
          case 'standard':
            this.responseType = 'code';
            break;
          case 'implicit':
            this.responseType = 'id_token token';
            break;
          case 'hybrid':
            this.responseType = 'code id_token token';
            break;
          default:
            throw 'Invalid value for flow';
        }
        this.flow = initOptions.flow;
      }

      if (initOptions.timeSkew != null) {
        this.timeSkew = initOptions.timeSkew;
      }

      if (initOptions.redirectUri) {
        this.redirectUri = initOptions.redirectUri;
      }

      if (initOptions.silentCheckSsoRedirectUri) {
        this.silentCheckSsoRedirectUri = initOptions.silentCheckSsoRedirectUri;
      }

      if (initOptions.pkceMethod) {
        if (initOptions.pkceMethod !== "S256") {
          throw 'Invalid value for pkceMethod';
        }
        this.pkceMethod = initOptions.pkceMethod;
      }

      if (typeof initOptions.enableLogging === 'boolean') {
        this.enableLogging = initOptions.enableLogging;
      } else {
        this.enableLogging = false;
      }
    }

    if (!this.responseMode) {
      this.responseMode = 'fragment';
    }
    if (!this.responseType) {
      this.responseType = 'code';
      this.flow = 'standard';
    }

    let promise = this.createPromise();

    let initPromise = this.createPromise();
    initPromise.promise.then(() => {
      this.onReady && this.onReady(this.authenticated);
      promise.setSuccess(this.authenticated);
    }).catch((errorData) => {
      promise.setError(errorData);
    });

    let configPromise = this.loadConfig();
    configPromise.then(this.processInit(initOptions, initPromise));
    configPromise.catch(() => {
      promise.setError();
    });

    return promise.promise;
  }

  login(options?: KeycloakLoginOptions): KeycloakPromise<void, void> {
    return this.adapter.login(options);
  }

  onLoad(initOptions: KeycloakInitOptions, initPromise) {
    let doLogin = (prompt) => {
      if (!prompt) {
        options.prompt = 'none';
      }

      this.login(options).then(() => {
        initPromise.setSuccess();
      }).catch(() => {
        initPromise.setError();
      });
    }

    let checkSsoSilently = () => {
      let ifrm = document.createElement("iframe");
      let src = this.createLoginUrl({prompt: 'none', redirectUri: this.silentCheckSsoRedirectUri});
      ifrm.setAttribute("src", src);
      ifrm.setAttribute("title", "keycloak-silent-check-sso");
      ifrm.style.display = "none";
      document.body.appendChild(ifrm);

      let messageCallback = (event) => {
        if (event.origin !== window.location.origin || ifrm.contentWindow !== event.source) {
          return;
        }

        let oauth = this.parseCallback(event.data);
        this.processCallback(oauth, initPromise);

        document.body.removeChild(ifrm);
        window.removeEventListener("message", messageCallback);
      };

      window.addEventListener("message", messageCallback);
    };

    let options = {
      prompt: undefined,
    };
    switch (initOptions.onLoad) {
      case 'check-sso':
        if (this.loginIframe.enable) {
          this.setupCheckLoginIframe().then(() => {
            this.checkLoginIframe().then((unchanged) => {
              if (!unchanged) {
                this.silentCheckSsoRedirectUri ? checkSsoSilently() : doLogin(false);
              } else {
                initPromise.setSuccess();
              }
            }).catch(() => {
              initPromise.setError();
            });
          });
        } else {
          this.silentCheckSsoRedirectUri ? checkSsoSilently() : doLogin(false);
        }
        break;
      case 'login-required':
        doLogin(true);
        break;
      default:
        throw 'Invalid value for onLoad';
    }
  }

  processInit(initOptions: KeycloakInitOptions, initPromise) {
    let callback = this.parseCallback(window.location.href);

    if (callback) {
      window.history.replaceState(window.history.state, null, callback.newUrl);
    }

    if (callback && callback.valid) {
      return this.setupCheckLoginIframe().then(() => {
        this.processCallback(callback, initPromise);
      }).catch((e) => {
        initPromise.setError(e);
      });
    } else if (initOptions) {
      if (initOptions.token && initOptions.refreshToken) {
        this.setToken(initOptions.token, initOptions.refreshToken, initOptions.idToken, null);

        if (this.loginIframe.enable) {
          this.setupCheckLoginIframe().then(() => {
            this.checkLoginIframe().then((unchanged) => {
              if (unchanged) {
                this.onAuthSuccess && this.onAuthSuccess();
                initPromise.setSuccess();
                this.scheduleCheckIframe();
              } else {
                initPromise.setSuccess();
              }
            }).catch(() => {
              initPromise.setError();
            });
          });
        } else {
          this.updateToken(-1).then(() => {
            this.onAuthSuccess && this.onAuthSuccess();
            initPromise.setSuccess();
          }).catch(() => {
            this.onAuthError && this.onAuthError();
            if (initOptions.onLoad) {
              this.onLoad(initOptions, initPromise);
            } else {
              initPromise.setError();
            }
          });
        }
      } else if (initOptions.onLoad) {
        this.onLoad(initOptions, initPromise);
      } else {
        initPromise.setSuccess();
      }
    } else {
      initPromise.setSuccess();
    }
  }

  generateRandomData(len) {
    // use web crypto APIs if possible
    let array;
    let crypto = window.crypto || window['msCrypto'];
    if (crypto && crypto.getRandomValues && window['Uint8Array']) {
      array = new Uint8Array(len);
      crypto.getRandomValues(array);
      return array;
    }

    // fallback to Math random
    array = new Array(len);
    for (let j = 0; j < array.length; j++) {
      array[j] = Math.floor(256 * Math.random());
    }
    return array;
  }

  generateCodeVerifier(len) {
    return this.generateRandomString(len, 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789');
  }

  generateRandomString(len, alphabet) {
    let randomData = this.generateRandomData(len);
    let chars = new Array(len);
    for (let i = 0; i < len; i++) {
      chars[i] = alphabet.charCodeAt(randomData[i] % alphabet.length);
    }
    return String.fromCharCode.apply(null, chars);
  }

  generatePkceChallenge(pkceMethod: KeycloakPkceMethod, codeVerifier) {
    switch (pkceMethod) {
      // The use of the "plain" method is considered insecure and therefore not supported.
      case "S256":
        // hash codeVerifier, then encode as url-safe base64 without padding
        let hashBytes = new Uint8Array(sha256.arrayBuffer(codeVerifier));
        let encodedHash = base64js.fromByteArray(hashBytes)
          .replace(/\+/g, '-')
          .replace(/\//g, '_')
          .replace(/\=/g, '');
        return encodedHash;
      default:
        throw 'Invalid value for pkceMethod';
    }
  }

  createLoginUrl(options?: KeycloakLoginOptions): string {
    let state = this.createUUID();
    let nonce = this.createUUID();

    let redirectUri = this.adapter.redirectUri(options);

    let callbackState = {
      state: state,
      nonce: nonce,
      redirectUri: encodeURIComponent(redirectUri),
      prompt: undefined,
      pkceCodeVerifier: undefined,
    };

    if (options && options.prompt) {
      callbackState.prompt = options.prompt;
    }

    let baseUrl;
    if (options && options.action == 'register') {
      baseUrl = this.endpoints.register();
    } else {
      baseUrl = this.endpoints.authorize();
    }

    let scope;
    if (options && options.scope) {
      if (options.scope.indexOf("openid") != -1) {
        scope = options.scope;
      } else {
        scope = "openid " + options.scope;
      }
    } else {
      scope = "openid";
    }

    let url = baseUrl
      + '?client_id=' + encodeURIComponent(this.clientId)
      + '&redirect_uri=' + encodeURIComponent(redirectUri)
      + '&state=' + encodeURIComponent(state)
      + '&response_mode=' + encodeURIComponent(this.responseMode)
      + '&response_type=' + encodeURIComponent(this.responseType)
      + '&scope=' + encodeURIComponent(scope);
    if (this.useNonce) {
      url = url + '&nonce=' + encodeURIComponent(nonce);
    }

    if (options && options.prompt) {
      url += '&prompt=' + encodeURIComponent(options.prompt);
    }

    if (options && options.maxAge) {
      url += '&max_age=' + encodeURIComponent(options.maxAge);
    }

    if (options && options.loginHint) {
      url += '&login_hint=' + encodeURIComponent(options.loginHint);
    }

    if (options && options.idpHint) {
      url += '&this_idp_hint=' + encodeURIComponent(options.idpHint);
    }

    if (options && options.action && options.action != 'register') {
      url += '&this_action=' + encodeURIComponent(options.action);
    }

    if (options && options.locale) {
      url += '&ui_locales=' + encodeURIComponent(options.locale);
    }

    if (this.pkceMethod) {
      let codeVerifier = this.generateCodeVerifier(96);
      callbackState.pkceCodeVerifier = codeVerifier;
      let pkceChallenge = this.generatePkceChallenge(this.pkceMethod, codeVerifier);
      url += '&code_challenge=' + pkceChallenge;
      url += '&code_challenge_method=' + this.pkceMethod;
    }

    this.callbackStorage.add(callbackState);

    return url;
  }

  logout(options?: KeycloakLogoutOptions): KeycloakPromise<void, void> {
    return this.adapter.logout(options);
  }

  createLogoutUrl(options?: KeycloakLogoutOptions): string {
    let url = this.endpoints.logout()
      + '?redirect_uri=' + encodeURIComponent(this.adapter.redirectUri(options, false));

    return url;
  }

  register(options?): KeycloakPromise<void, void> {
    return this.adapter.register(options);
  }

  createRegisterUrl(options?: KeycloakLoginOptions): string {
    if (!options) {
      options = {};
    }
    options.action = 'register';
    return this.createLoginUrl(options);
  }

  createAccountUrl(options?): string {
    let realm = this.getRealmUrl();
    let url = undefined;
    if (typeof realm !== 'undefined') {
      url = realm
        + '/account'
        + '?referrer=' + encodeURIComponent(this.clientId)
        + '&referrer_uri=' + encodeURIComponent(this.adapter.redirectUri(options));
    }
    return url;
  }

  accountManagement(): KeycloakPromise<void, void> {
    return this.adapter.accountManagement();
  }

  hasRealmRole(role: string): boolean {
    let access = this.realmAccess;
    return !!access && access.roles.indexOf(role) >= 0;
  }

  hasResourceRole(role: string, resource?: string): boolean {
    if (!this.resourceAccess) {
      return false;
    }

    let access = this.resourceAccess[resource || this.clientId];
    return !!access && access.roles.indexOf(role) >= 0;
  }

  loadUserProfile(): KeycloakPromise<KeycloakProfile, void> {
    let url = this.getRealmUrl() + '/account';
    let req = new XMLHttpRequest();
    req.open('GET', url, true);
    req.setRequestHeader('Accept', 'application/json');
    req.setRequestHeader('Authorization', 'bearer ' + this.token);

    let promise = this.createPromise();

    req.onreadystatechange = () => {
      if (req.readyState == 4) {
        if (req.status == 200) {
          this.profile = JSON.parse(req.responseText);
          promise.setSuccess(this.profile);
        } else {
          promise.setError();
        }
      }
    }

    req.send();

    return promise.promise;
  }

  loadUserInfo(): KeycloakPromise<{}, void> {
    let url = this.endpoints.userinfo();
    let req = new XMLHttpRequest();
    req.open('GET', url, true);
    req.setRequestHeader('Accept', 'application/json');
    req.setRequestHeader('Authorization', 'bearer ' + this.token);

    let promise = this.createPromise();

    req.onreadystatechange = () => {
      if (req.readyState == 4) {
        if (req.status == 200) {
          this.userInfo = JSON.parse(req.responseText);
          promise.setSuccess(this.userInfo);
        } else {
          promise.setError();
        }
      }
    }

    req.send();

    return promise.promise;
  }

  isTokenExpired(minValidity?: number): boolean {
    if (!this.tokenParsed || (!this.refreshToken && this.flow != 'implicit')) {
      throw 'Not authenticated';
    }

    if (this.timeSkew == null) {
      this.logInfo('[KEYCLOAK] Unable to determine if token is expired as timeskew is not set');
      return true;
    }

    let expiresIn = this.tokenParsed['exp'] - Math.ceil(new Date().getTime() / 1000) + this.timeSkew;
    if (minValidity) {
      if (isNaN(minValidity)) {
        throw 'Invalid minValidity';
      }
      expiresIn -= minValidity;
    }
    return expiresIn < 0;
  }

  updateToken(minValidity: number): KeycloakPromise<boolean, boolean> {
    let promise = this.createPromise();

    if (!this.refreshToken) {
      promise.setError();
      return promise.promise;
    }

    minValidity = minValidity || 5;

    let exec = () => {
      let refreshToken = false;
      if (minValidity == -1) {
        refreshToken = true;
        this.logInfo('[KEYCLOAK] Refreshing token: forced refresh');
      } else if (!this.tokenParsed || this.isTokenExpired(minValidity)) {
        refreshToken = true;
        this.logInfo('[KEYCLOAK] Refreshing token: token expired');
      }

      if (!refreshToken) {
        promise.setSuccess(false);
      } else {
        let params = 'grant_type=refresh_token&' + 'refresh_token=' + this.refreshToken;
        let url = this.endpoints.token();

        this.refreshQueue.push(promise);

        if (this.refreshQueue.length == 1) {
          let req = new XMLHttpRequest();
          req.open('POST', url, true);
          req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
          req.withCredentials = true;

          params += '&client_id=' + encodeURIComponent(this.clientId);

          let timeLocal = new Date().getTime();

          req.onreadystatechange = () => {
            if (req.readyState == 4) {
              if (req.status == 200) {
                this.logInfo('[KEYCLOAK] Token refreshed');

                timeLocal = (timeLocal + new Date().getTime()) / 2;

                let tokenResponse = JSON.parse(req.responseText);

                this.setToken(tokenResponse['access_token'], tokenResponse['refresh_token'], tokenResponse['id_token'], timeLocal);

                this.onAuthRefreshSuccess && this.onAuthRefreshSuccess();
                for (let p = this.refreshQueue.pop(); p != null; p = this.refreshQueue.pop()) {
                  p.setSuccess(true);
                }
              } else {
                this.logWarn('[KEYCLOAK] Failed to refresh token');

                if (req.status == 400) {
                  this.clearToken();
                }

                this.onAuthRefreshError && this.onAuthRefreshError();
                for (let p = this.refreshQueue.pop(); p != null; p = this.refreshQueue.pop()) {
                  p.setError(true);
                }
              }
            }
          };

          req.send(params);
        }
      }
    }

    if (this.loginIframe.enable) {
      let iframePromise = this.checkLoginIframe();
      iframePromise.then(() => {
        exec();
      }).catch(() => {
        promise.setError();
      });
    } else {
      exec();
    }

    return promise.promise;
  }

  clearToken(): void {
    if (this.token) {
      this.setToken(null, null, null, null);
      this.onAuthLogout && this.onAuthLogout();
      if (this.loginRequired) {
        this.login();
      }
    }
  }

  getRealmUrl() {
    if (typeof this.authServerUrl !== 'undefined') {
      if (this.authServerUrl.charAt(this.authServerUrl.length - 1) == '/') {
        return this.authServerUrl + 'realms/' + encodeURIComponent(this.realm);
      } else {
        return this.authServerUrl + '/realms/' + encodeURIComponent(this.realm);
      }
    } else {
      return undefined;
    }
  }

  getOrigin() {
    if (!window.location.origin) {
      return window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
    } else {
      return window.location.origin;
    }
  }

  processCallback(oauth, promise) {
    let code = oauth.code;
    let error = oauth.error;
    let prompt = oauth.prompt;

    let timeLocal = new Date().getTime();

    if (oauth['kc_action_status']) {
      this.onActionUpdate && this.onActionUpdate(oauth['kc_action_status']);
    }

    if (error) {
      if (prompt != 'none') {
        let errorData = {error: error, error_description: oauth.error_description};
        this.onAuthError && this.onAuthError(errorData);
        promise && promise.setError(errorData);
      } else {
        promise && promise.setSuccess();
      }
      return;
    } else if ((this.flow != 'standard') && (oauth.access_token || oauth.id_token)) {
      this.authSuccess(oauth.access_token, null, oauth.id_token, true, timeLocal, oauth, promise);
    }

    if ((this.flow != 'implicit') && code) {
      let params = 'code=' + code + '&grant_type=authorization_code';
      let url = this.endpoints.token();

      let req = new XMLHttpRequest();
      req.open('POST', url, true);
      req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

      params += '&client_id=' + encodeURIComponent(this.clientId);
      params += '&redirect_uri=' + oauth.redirectUri;

      if (oauth.pkceCodeVerifier) {
        params += '&code_verifier=' + oauth.pkceCodeVerifier;
      }

      req.withCredentials = true;

      req.onreadystatechange = () => {
        if (req.readyState == 4) {
          if (req.status == 200) {

            let tokenResponse = JSON.parse(req.responseText);
            this.authSuccess(
              tokenResponse['access_token'],
              tokenResponse['refresh_token'],
              tokenResponse['id_token'],
              this.flow === 'standard',
              timeLocal,
              oauth,
              promise);
            this.scheduleCheckIframe();
          } else {
            this.onAuthError && this.onAuthError();
            promise && promise.setError();
          }
        }
      };

      req.send(params);
    }


  }

  authSuccess(accessToken, refreshToken, idToken, fulfillPromise, timeLocal, oauth, promise) {
    timeLocal = (timeLocal + new Date().getTime()) / 2;

    this.setToken(accessToken, refreshToken, idToken, timeLocal);

    if (this.useNonce && ((this.tokenParsed && this.tokenParsed.nonce != oauth.storedNonce) ||
      (this.refreshTokenParsed && this.refreshTokenParsed.nonce != oauth.storedNonce) ||
      (this.idTokenParsed && this.idTokenParsed.nonce != oauth.storedNonce))) {

      this.logInfo('[KEYCLOAK] Invalid nonce, clearing token');
      this.clearToken();
      promise && promise.setError();
    } else {
      if (fulfillPromise) {
        this.onAuthSuccess && this.onAuthSuccess();
        promise && promise.setSuccess();
      }
    }
  }

  loadConfig() {
    let promise = this.createPromise();
    let configUrl;

    if (!this.config) {
      configUrl = 'keycloak.json';
    } else if (typeof this.config === 'string') {
      configUrl = this.config;
    }

    if (configUrl) {
      let req = new XMLHttpRequest();
      req.open('GET', configUrl, true);
      req.setRequestHeader('Accept', 'application/json');

      req.onreadystatechange = () => {
        if (req.readyState == 4) {
          if (req.status == 200 || this.fileLoaded(req)) {
            let config = JSON.parse(req.responseText);

            this.authServerUrl = config['auth-server-url'];
            this.realm = config['realm'];
            this.clientId = config['resource'];
            this.setupOidcEndoints(null);
            promise.setSuccess();
          } else {
            promise.setError();
          }
        }
      };

      req.send();
    } else {
      const config: KeycloakConfig = this.config as KeycloakConfig
      if (!config.clientId) {
        throw 'clientId missing';
      }

      this.clientId = config.clientId;

      let oidcProvider = config['oidcProvider'];
      if (!oidcProvider) {
        if (!config['url']) {
          let scripts = document.getElementsByTagName('script');
          for (let i = 0; i < scripts.length; i++) {
            if (scripts[i].src.match(/.*keycloak\.js/)) {
              config.url = scripts[i].src.substr(0, scripts[i].src.indexOf('/js/keycloak.js'));
              break;
            }
          }
        }
        if (!config.realm) {
          throw 'realm missing';
        }

        this.authServerUrl = config.url;
        this.realm = config.realm;
        this.setupOidcEndoints(null);
        promise.setSuccess();
      } else {
        if (typeof oidcProvider === 'string') {
          let oidcProviderConfigUrl;
          if (oidcProvider.charAt(oidcProvider.length - 1) == '/') {
            oidcProviderConfigUrl = oidcProvider + '.well-known/openid-configuration';
          } else {
            oidcProviderConfigUrl = oidcProvider + '/.well-known/openid-configuration';
          }
          let req = new XMLHttpRequest();
          req.open('GET', oidcProviderConfigUrl, true);
          req.setRequestHeader('Accept', 'application/json');

          req.onreadystatechange = () => {
            if (req.readyState == 4) {
              if (req.status == 200 || this.fileLoaded(req)) {
                let oidcProviderConfig = JSON.parse(req.responseText);
                this.setupOidcEndoints(oidcProviderConfig);
                promise.setSuccess();
              } else {
                promise.setError();
              }
            }
          };

          req.send();
        } else {
          this.setupOidcEndoints(oidcProvider);
          promise.setSuccess();
        }
      }
    }

    return promise.promise;
  }

  setupOidcEndoints(oidcConfiguration) {
    if (!oidcConfiguration) {
      this.endpoints = {
        authorize: () => {
          return this.getRealmUrl() + '/protocol/openid-connect/auth';
        },
        token: () => {
          return this.getRealmUrl() + '/protocol/openid-connect/token';
        },
        logout: () => {
          return this.getRealmUrl() + '/protocol/openid-connect/logout';
        },
        checkSessionIframe: () => {
          let src = this.getRealmUrl() + '/protocol/openid-connect/login-status-iframe.html';
          if (this.iframeVersion) {
            src = src + '?version=' + this.iframeVersion;
          }
          return src;
        },
        register: () => {
          return this.getRealmUrl() + '/protocol/openid-connect/registrations';
        },
        userinfo: () => {
          return this.getRealmUrl() + '/protocol/openid-connect/userinfo';
        },
      };
    } else {
      this.endpoints = {
        authorize: () => {
          return oidcConfiguration.authorization_endpoint;
        },
        token: () => {
          return oidcConfiguration.token_endpoint;
        },
        logout: () => {
          if (!oidcConfiguration.end_session_endpoint) {
            throw "Not supported by the OIDC server";
          }
          return oidcConfiguration.end_session_endpoint;
        },
        checkSessionIframe: () => {
          if (!oidcConfiguration.check_session_iframe) {
            throw "Not supported by the OIDC server";
          }
          return oidcConfiguration.check_session_iframe;
        },
        register: () => {
          throw 'Redirection to "Register user" page not supported in standard OIDC mode';
        },
        userinfo: () => {
          if (!oidcConfiguration.userinfo_endpoint) {
            throw "Not supported by the OIDC server";
          }
          return oidcConfiguration.userinfo_endpoint;
        },
      }
    }
  }

  fileLoaded(xhr) {
    return xhr.status == 0 && xhr.responseText && xhr.responseURL.startsWith('file:');
  }

  setToken(token, refreshToken, idToken, timeLocal) {
    if (this.tokenTimeoutHandle) {
      clearTimeout(this.tokenTimeoutHandle);
      this.tokenTimeoutHandle = null;
    }

    if (refreshToken) {
      this.refreshToken = refreshToken;
      this.refreshTokenParsed = this.decodeToken(refreshToken);
    } else {
      delete this.refreshToken;
      delete this.refreshTokenParsed;
    }

    if (idToken) {
      this.idToken = idToken;
      this.idTokenParsed = this.decodeToken(idToken);
    } else {
      delete this.idToken;
      delete this.idTokenParsed;
    }

    if (token) {
      this.token = token;
      this.tokenParsed = this.decodeToken(token);
      this.sessionId = this.tokenParsed.session_state;
      this.authenticated = true;
      this.subject = this.tokenParsed.sub;
      this.realmAccess = this.tokenParsed.realm_access;
      this.resourceAccess = this.tokenParsed.resource_access;

      if (timeLocal) {
        this.timeSkew = Math.floor(timeLocal / 1000) - this.tokenParsed.iat;
      }

      if (this.timeSkew != null) {
        this.logInfo('[KEYCLOAK] Estimated time difference between browser and server is ' + this.timeSkew + ' seconds');

        if (this.onTokenExpired) {
          let expiresIn = (this.tokenParsed['exp'] - (new Date().getTime() / 1000) + this.timeSkew) * 1000;
          this.logInfo('[KEYCLOAK] Token expires in ' + Math.round(expiresIn / 1000) + ' s');
          if (expiresIn <= 0) {
            this.onTokenExpired();
          } else {
            this.tokenTimeoutHandle = setTimeout(this.onTokenExpired, expiresIn);
          }
        }
      }
    } else {
      delete this.token;
      delete this.tokenParsed;
      delete this.subject;
      delete this.realmAccess;
      delete this.resourceAccess;

      this.authenticated = false;
    }
  }

  decodeToken(str) {
    str = str.split('.')[1];

    str = str.replace('/-/g', '+');
    str = str.replace('/_/g', '/');
    switch (str.length % 4) {
      case 0:
        break;
      case 2:
        str += '==';
        break;
      case 3:
        str += '=';
        break;
      default:
        throw 'Invalid token';
    }

    str = (str + '===').slice(0, str.length + (str.length % 4));
    str = str.replace(/-/g, '+').replace(/_/g, '/');

    str = decodeURIComponent(escape(atob(str)));

    str = JSON.parse(str);
    return str;
  }

  createUUID() {
    let hexDigits = '0123456789abcdef';
    let s = this.generateRandomString(36, hexDigits).split("");
    s[14] = '4';
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);
    s[8] = s[13] = s[18] = s[23] = '-';
    let uuid = s.join('');
    return uuid;
  }

  parseCallback(url) {
    let oauth = this.parseCallbackUrl(url);
    if (!oauth) {
      return;
    }

    let oauthState = this.callbackStorage.get(oauth.state);

    if (oauthState) {
      oauth.valid = true;
      oauth.redirectUri = oauthState.redirectUri;
      oauth.storedNonce = oauthState.nonce;
      oauth.prompt = oauthState.prompt;
      oauth.pkceCodeVerifier = oauthState.pkceCodeVerifier;
    }

    return oauth;
  }

  parseCallbackUrl(url) {
    let supportedParams;
    switch (this.flow) {
      case 'standard':
        supportedParams = ['code', 'state', 'session_state', 'this_action_status'];
        break;
      case 'implicit':
        supportedParams = ['access_token', 'token_type', 'id_token', 'state', 'session_state', 'expires_in', 'this_action_status'];
        break;
      case 'hybrid':
        supportedParams = ['access_token', 'id_token', 'code', 'state', 'session_state', 'this_action_status'];
        break;
    }

    supportedParams.push('error');
    supportedParams.push('error_description');
    supportedParams.push('error_uri');

    let queryIndex = url.indexOf('?');
    let fragmentIndex = url.indexOf('#');

    let newUrl;
    let parsed;

    if (this.responseMode === 'query' && queryIndex !== -1) {
      newUrl = url.substring(0, queryIndex);
      parsed = this.parseCallbackParams(url.substring(queryIndex + 1, fragmentIndex !== -1 ? fragmentIndex : url.length), supportedParams);
      if (parsed.paramsString !== '') {
        newUrl += '?' + parsed.paramsString;
      }
      if (fragmentIndex !== -1) {
        newUrl += url.substring(fragmentIndex);
      }
    } else if (this.responseMode === 'fragment' && fragmentIndex !== -1) {
      newUrl = url.substring(0, fragmentIndex);
      parsed = this.parseCallbackParams(url.substring(fragmentIndex + 1), supportedParams);
      if (parsed.paramsString !== '') {
        newUrl += '#' + parsed.paramsString;
      }
    }

    if (parsed && parsed.oauthParams) {
      if (this.flow === 'standard' || this.flow === 'hybrid') {
        if ((parsed.oauthParams.code || parsed.oauthParams.error) && parsed.oauthParams.state) {
          parsed.oauthParams.newUrl = newUrl;
          return parsed.oauthParams;
        }
      } else if (this.flow === 'implicit') {
        if ((parsed.oauthParams.access_token || parsed.oauthParams.error) && parsed.oauthParams.state) {
          parsed.oauthParams.newUrl = newUrl;
          return parsed.oauthParams;
        }
      }
    }
  }

  parseCallbackParams(paramsString, supportedParams) {
    let p = paramsString.split('&');
    let result = {
      paramsString: '',
      oauthParams: {},
    }
    for (let i = 0; i < p.length; i++) {
      let split = p[i].indexOf("=");
      let key = p[i].slice(0, split);
      if (supportedParams.indexOf(key) !== -1) {
        result.oauthParams[key] = p[i].slice(split + 1);
      } else {
        if (result.paramsString !== '') {
          result.paramsString += '&';
        }
        result.paramsString += p[i];
      }
    }
    return result;
  }

  createPromise() {
    // Need to create a native Promise which also preserves the
    // interface of the custom promise type previously used by the API
    let p = {
      setSuccess: (result?) => {
        p.resolve(result);
      },
      setError: (result?) => {
        p.reject(result);
      },
      resolve: undefined,
      reject: undefined,
      promise: undefined,
    };
    p.promise = new KeycloakPromise((resolve, reject) => {
      p.resolve = resolve;
      p.reject = reject;
    });
    return p;
  }

  setupCheckLoginIframe() {
    let promise = this.createPromise();

    if (!this.loginIframe.enable) {
      promise.setSuccess();
      return promise.promise;
    }

    if (this.loginIframe.iframe) {
      promise.setSuccess();
      return promise.promise;
    }

    let iframe = document.createElement('iframe');
    this.loginIframe.iframe = iframe;

    iframe.onload = () => {
      let authUrl = this.endpoints.authorize();
      if (authUrl.charAt(0) === '/') {
        this.loginIframe.iframeOrigin = this.getOrigin();
      } else {
        this.loginIframe.iframeOrigin = authUrl.substring(0, authUrl.indexOf('/', 8));
      }
      promise.setSuccess();
    }

    let src = this.endpoints.checkSessionIframe();
    iframe.setAttribute('src', src);
    iframe.setAttribute('title', 'keycloak-session-iframe');
    iframe.style.display = 'none';
    document.body.appendChild(iframe);

    let messageCallback = (event) => {
      if ((event.origin !== this.loginIframe.iframeOrigin) || (this.loginIframe.iframe.contentWindow !== event.source)) {
        return;
      }

      if (!(event.data == 'unchanged' || event.data == 'changed' || event.data == 'error')) {
        return;
      }


      if (event.data != 'unchanged') {
        this.clearToken();
      }

      let callbacks = this.loginIframe.callbackList.splice(0, this.loginIframe.callbackList.length);

      for (let i = callbacks.length - 1; i >= 0; --i) {
        let promise = callbacks[i];
        if (event.data == 'error') {
          promise.setError();
        } else {
          promise.setSuccess(event.data == 'unchanged');
        }
      }
    };

    window.addEventListener('message', messageCallback, false);

    return promise.promise;
  }

  scheduleCheckIframe() {
    if (this.loginIframe.enable) {
      if (this.token) {
        setTimeout(() => {
          this.checkLoginIframe().then((unchanged) => {
            if (unchanged) {
              this.scheduleCheckIframe();
            }
          });
        }, this.loginIframe.interval * 1000);
      }
    }
  }

  checkLoginIframe() {
    let promise = this.createPromise();

    if (this.loginIframe.iframe && this.loginIframe.iframeOrigin) {
      let msg = this.clientId + ' ' + (this.sessionId ? this.sessionId : '');
      this.loginIframe.callbackList.push(promise);
      let origin = this.loginIframe.iframeOrigin;
      if (this.loginIframe.callbackList.length == 1) {
        this.loginIframe.iframe.contentWindow.postMessage(msg, origin);
      }
    } else {
      promise.setSuccess();
    }

    return promise.promise;
  }

  loadAdapter(type?): KeycloakAdapter {
    if (!type || type == 'default') {
      return {
        login: (options?: KeycloakLoginOptions) => {
          window.location.replace(this.createLoginUrl(options));
          return this.createPromise().promise;
        },

        logout: (options?: KeycloakLogoutOptions) => {
          window.location.replace(this.createLogoutUrl(options));
          return this.createPromise().promise;
        },

        register: (options?: KeycloakLoginOptions) => {
          window.location.replace(this.createRegisterUrl(options));
          return this.createPromise().promise;
        },

        accountManagement: () => {
          let accountUrl = this.createAccountUrl();
          if (typeof accountUrl !== 'undefined') {
            window.location.href = accountUrl;
          } else {
            throw "Not supported by the OIDC server";
          }
          return this.createPromise().promise;
        },

        redirectUri: (options, encodeHash) => {
          if (encodeHash === undefined || encodeHash === null) {
            encodeHash = true;
          }

          if (options && options.redirectUri) {
            return options.redirectUri;
          } else if (this.redirectUri) {
            return this.redirectUri;
          } else {
            return location.href;
          }
        },
      };
    }


    throw 'invalid adapter type: ' + type;
  }

  createCallbackStorage(): LocalStorage | CookieStorage {
    try {
      return new LocalStorage();
    } catch (err) {
    }

    return new CookieStorage();
  }

  createLogger(fn) {
    return (...args) => {
      if (this.enableLogging) {
        fn.apply(console, Array.prototype.slice.call(args));
      }
    };
  }

  isAuthenticated(): boolean {
    return this.authenticated;
  }

  getToken(): string {
    return this.token;
  }

  /**
   * Return the roles of the logged user. The allRoles parameter, with default value
   * true, will return the clientId and realm roles associated with the logged user. If set to false
   * it will only return the user roles associated with the clientId.
   *
   * @param allRoles
   * Flag to set if all roles should be returned.(Optional: default value is true)
   * @returns
   * Array of Roles associated with the logged user.
   */
  getUserRoles(allRoles: boolean = true): string[] {
    let roles: string[] = [];
    if (this.resourceAccess) {
      for (const key in this.resourceAccess) {
        if (this.resourceAccess.hasOwnProperty(key)) {
          const resourceAccess: any = this.resourceAccess[key];
          const clientRoles = resourceAccess['roles'] || [];
          roles = roles.concat(clientRoles);
        }
      }
    }
    if (allRoles && this.realmAccess) {
      const realmRoles = this.realmAccess['roles'] || [];
      roles.push(...realmRoles);
    }
    return roles;
  }

  /**
   * Check if the user has access to the specified role. It will look for roles in
   * realm and clientId, but will not check if the user is logged in for better performance.
   *
   * @param role
   * role name
   * @param resource
   * resource name If not specified, `clientId` is used
   * @returns
   * A boolean meaning if the user has the specified Role.
   */
  isUserInRole(role: string, resource?: string): boolean {
    let hasRole: boolean;
    hasRole = this.hasResourceRole(role, resource);
    if (!hasRole) {
      hasRole = this.hasRealmRole(role);
    }
    return hasRole;
  }
}

