import { forwardRef, ModuleWithProviders, NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { BearerInterceptor } from './interceptor';

@NgModule({
  imports: [
    HttpClientModule,
  ],
})
export class AngularKeycloakModule {
  static forRoot(): ModuleWithProviders<AngularKeycloakModule> {
    return {
      ngModule: AngularKeycloakModule,
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: forwardRef(() => BearerInterceptor),
          multi: true,
        },
      ],

    }
  }
}
